package com.songoda.skyblock.island;

public enum IslandRole {

    Coop, Visitor, Member, Operator, Owner

}
