package com.songoda.skyblock.cooldown;

public enum CooldownType {

    Biome, Creation, Levelling, Ownership, Teleport

}
